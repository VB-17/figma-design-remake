export default [
  {
    idx: "01",
    heading: "What level of hiker are you?",
    subHeading: "Get Started",
    description:
      "Determining what level of hiker you are can be on important tool when planning future hikes. This hiking level guide will help you plan hikes according to different hike ratings set by various websites like All Trails and Modern Hiker. What type of hiker are you - novice, moderate, advanced moderate, expert or expoert backpacker ?",
  },
  {
    idx: "02",
    heading: "Picking the right Hiking Gear!",
    subHeading: "Hiking Essentials",
    description:
      "The nice thing about beginning hiking is that you don't really need any specails gear, you can probably get away with things you already have. Let's starat with clothing. A typical mistake hiking beginneres make is wearking jeans and regular clothes, which will get heave and chafe wif they get sweaty or wet.",
  },
  {
    idx: "03",
    heading: "Understand your Map & Timing",
    subHeading: "Where you go is the key",
    description:
      "To start, print out the hiking guide and map. If it's raining, throw them ina Zip-Lock bag. Read over the guide, study the map, and have a good idea of what to expect. I like to know what my next landmark is as I hike. For example, I'll read the guide and know that say, in a mile, I make a right turn at the junction.",
  },
];
